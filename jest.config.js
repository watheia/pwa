module.exports = {
  projects: [
    "<rootDir>/apps/pwa",
    "<rootDir>/libs/base-ui/atoms",
    "<rootDir>/apps/home",
    "<rootDir>/apps/blog",
    "<rootDir>/libs/dato",
    "<rootDir>/libs/pwa",
    "<rootDir>/libs/base-ui",
  ],
}
