FROM gitpod/workspace-full

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

ENV PATH=$PATH:$HOME/bin
RUN echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc

# Upgrade base sys
RUN sudo apt update && \
    sudo apt upgrade -y && \
    sudo apt install graphviz -y && \

# Install deps + add Chrome Stable + purge all the things
COPY scripts/install-chrome.sh /tmp/install-chrome.sh

RUN sudo chmod +x /tmp/install-chrome.sh && \
  sudo bash -C /tmp/install-chrome.sh && \
  rm /tmp/install-chrome.sh

# Add Chrome as a user
RUN groupadd -r chrome && useradd -r -g chrome -G audio,video chrome \
  && mkdir -p /home/chrome && chown -R chrome:chrome /home/chrome

# start bit dev server
RUN npm install --global yarn npm prettier eslint jest @teambit/bvm && \
    export PATH=$PATH:$HOME/bin && \
    bvm install && \
    bit config set analytics_reporting false && \
    bit config set error_reporting false && \
    bit config set no_warnings true && \
    bit init --harmony && \
    bit import && \
    bit compile && \
    bit install && \
    bit start &
