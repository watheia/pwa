import React from "react"
import { ColorBorder } from "./color-border"

export const ColorBorderDefault = () => <ColorBorder color="#babec9" />
