import React from "react"
import { IconButton } from "./icon-button"

export function IconButtonExample() {
  return (
    <IconButton id="icon-and-text" icon="error-circle">
      Terminate
    </IconButton>
  )
}

export function IconButtonDisabled() {
  return (
    <IconButton id="disabled-icon-button" icon="error-circle" disabled>
      Terminate
    </IconButton>
  )
}

export function IconOnlyButtonExample() {
  return <IconButton id="icon-only" icon="error-circle"></IconButton>
}

export function TextOnlyButtonExample() {
  return <IconButton>Terminate</IconButton>
}

export function LoadingButtonExample() {
  return <IconButton loading />
}

export function ActiveButtonExample() {
  return <IconButton active>Terminate</IconButton>
}

export function ActiveWithIconButtonExample() {
  return (
    <IconButton active icon="error-circle">
      Terminate
    </IconButton>
  )
}
