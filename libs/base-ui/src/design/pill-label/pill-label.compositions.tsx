import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { PillLabel } from "./pill-label"

export const PillLabelExample = () => (
  <ThemeProvider>
    <PillLabel>This is a pill-shaped label</PillLabel>
  </ThemeProvider>
)
