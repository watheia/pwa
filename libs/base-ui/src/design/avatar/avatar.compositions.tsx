import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { DefaultAvatar, OrgAvatar, UserAvatar } from "./index"

const accounts = {
  defAccount: {
    name: "defaultAccount",
    type: "default",
    profileImage: "https://static.bit.dev/harmony/support.svg",
  },
  orgAccount: {
    name: "defaultAccount",
    type: "organization",
    profileImage: "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-logo.svg",
  },
  userAccount: {
    displayName: "display name",
    name: "defaultAccount",
    type: "user",
    profileImage: "https://static.bit.dev/harmony/github.svg",
  },
  noPicOrgAccount: { name: "defaultAccount", type: "organization" },
  noPicUserAccount: { name: "defaultAccount", type: "user" },
  noNameAccount: { name: "", type: "user" },
}

export const DefaultAvatarExample = () => (
  <ThemeProvider>
    <DefaultAvatar size={32} account={accounts.defAccount} />
  </ThemeProvider>
)

export const OrganizationAvatarExample = () => (
  <ThemeProvider>
    <OrgAvatar size={32} account={accounts.orgAccount} />
  </ThemeProvider>
)

export const UserAvatarExample = () => (
  <ThemeProvider>
    <UserAvatar size={32} account={accounts.userAccount} />
  </ThemeProvider>
)

export const LargeAvatarExample = () => (
  <ThemeProvider>
    <OrgAvatar size={100} account={accounts.orgAccount} />
  </ThemeProvider>
)

export const NoSetIconOrgAvatar = () => (
  <ThemeProvider>
    <OrgAvatar size={32} account={accounts.noPicOrgAccount} />
  </ThemeProvider>
)

export const NoSetIconUserAvatar = () => (
  <ThemeProvider>
    <UserAvatar size={32} account={accounts.noPicUserAccount} />
  </ThemeProvider>
)

export const NoUserNameAvatarExample = () => (
  <ThemeProvider>
    <UserAvatar size={32} account={accounts.noNameAccount} />
  </ThemeProvider>
)

export const UserAvatarWithTooltipExample = () => (
  <ThemeProvider>
    <UserAvatar size={32} account={accounts.userAccount} showTooltip />
  </ThemeProvider>
)
