import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ServerErrorPage } from "./server-error-page"

export const ServerErrorPageExample = () => {
  return (
    <ThemeProvider>
      <ServerErrorPage />
    </ThemeProvider>
  )
}
