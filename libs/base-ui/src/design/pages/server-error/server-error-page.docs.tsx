import React from "react"
import { Section } from "@watheia/base-ui.mdx.ui.section"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Separator } from "@watheia/base-ui.mdx.ui.separator"
import { ServerErrorPage } from "./server-error-page"

export default function Overview() {
  return (
    <ThemeProvider>
      <>
        <Section>A page component that displays a 500 error message.</Section>
        <Separator />
      </>
    </ThemeProvider>
  )
}

Overview.abstract = "A 500 error page."

Overview.labels = ["react", "typescript", "500", "error", "page"]

Overview.examples = [
  {
    scope: {
      ServerErrorPage,
    },
    title: "500 page",
    description: "500 error message",
    jsx: <ServerErrorPage />,
  },
]
