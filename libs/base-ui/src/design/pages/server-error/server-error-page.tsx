import React from "react"
import { ExternalLink } from "@watheia/base-ui.design.external-link"
import { Icon } from "@watheia/base-ui.atoms.icon"
import { ErrorPage } from "@watheia/base-ui.design.error-page"
import styles from "./server-error-page.module.scss"

type ServerErrorPageProps = {} & React.HTMLAttributes<HTMLDivElement>

export function ServerErrorPage({ ...rest }: ServerErrorPageProps) {
  return (
    <ErrorPage {...rest} code={500} title="Internal server error">
      <div className={styles.iconLine}>
        <ExternalLink href="https://gitlab.com/watheia/mce">
          <Icon of="github-logo" className={styles.github} />
        </ExternalLink>
        <ExternalLink href="https://watheia.io/">
          <img
            alt="wa-logo"
            className={styles.logo}
            src="https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-logo.svg"
          />
        </ExternalLink>
      </div>
    </ErrorPage>
  )
}
