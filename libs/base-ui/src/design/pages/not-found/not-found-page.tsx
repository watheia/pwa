import React from "react"
import { Icon } from "@watheia/base-ui.atoms.icon"
import { ErrorPage } from "@watheia/base-ui.design.error-page"
import { ExternalLink } from "@watheia/base-ui.design.external-link"
import styles from "./not-found-page.module.scss"

export type NotFoundPageProps = {} & React.HTMLAttributes<HTMLDivElement>

export function NotFoundPage({ ...rest }: NotFoundPageProps) {
  return (
    <ErrorPage {...rest} code={404} title="Page not found">
      <div className={styles.iconLine}>
        <ExternalLink href="https://gitlab.com/watheia/mce">
          <Icon of="github-logo" className={styles.github} />
        </ExternalLink>
        <ExternalLink href="https://watheia.io/">
          <img
            alt="wa-logo"
            className={styles.logo}
            src="https://gitlab.com/watheia/pwa/-/raw/main/assets/favicon-32x32.png"
          />
        </ExternalLink>
      </div>
    </ErrorPage>
  )
}
