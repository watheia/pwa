import React, { useState } from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Label, LabelList } from "./label"

export function Single() {
  const [state, setState] = useState("")

  return (
    <ThemeProvider>
      <Label onPick={setState}>chart</Label>
      {state && <div>label "{state}" chosen</div>}
    </ThemeProvider>
  )
}

export function List() {
  const [state, setState] = useState("")

  return (
    <ThemeProvider>
      <LabelList onPick={setState}>{["chart", "graph", "ui-component", "react"]}</LabelList>
      {state && <div>label "{state}" chosen</div>}
    </ThemeProvider>
  )
}
