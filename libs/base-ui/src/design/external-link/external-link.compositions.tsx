import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ExternalLink } from "./external-link"

export const ExternalLinkExample = () => (
  <ThemeProvider>
    <ExternalLink href="https://watheia.io">
      some link text - take me to watheia.io!!
    </ExternalLink>
  </ThemeProvider>
)
