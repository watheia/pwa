import React from "react"
import { Paper } from "./paper"

export const DefaultPaper = () => (
  <Paper>
    <p>Hello, World!</p>
  </Paper>
)
