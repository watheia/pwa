import React from "react"
import { DarkTheme, LightTheme } from "./theme.composition"
import { render } from "@testing-library/react"

it("should render the light theme", () => {
  const { getByText } = render(<LightTheme />)
  const rendered = getByText("this should be light")
  expect(rendered).toBeTruthy()
})

it("should render the dark theme", () => {
  const { getByText } = render(<DarkTheme />)
  const rendered = getByText("this should be dark")
  expect(rendered).toBeTruthy()
})
