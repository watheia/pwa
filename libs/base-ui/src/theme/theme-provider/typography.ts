export const fontWeightLight = 300

export const fontWeightRegular = 400

export const fontWeightMedium = 700

export const fontFamily = "source-sans-pro, sans-serif"

export const fontFamilySecondary = "'Roboto', sans-serif"

export const typography = {
  fontFamily,
  fontSize: 14,
  fontWeightLight,
  fontWeightRegular,
  fontWeightMedium,
  fontFamilySecondary,
  h1: {
    letterSpacing: 0,
    fontSize: 60,
  },
  h2: {
    fontSize: 48,
  },
  h3: {
    fontSize: 42,
  },
  h4: {
    fontSize: 36,
  },
  h5: {
    fontSize: 20,
    fontWeight: fontWeightLight,
  },
  h6: {
    fontSize: 18,
  },
  subtitle1: {
    fontSize: 18,
  },
  body1: {
    fontWeight: fontWeightRegular,
    fontSize: 16,
  },
  body2: {
    fontSize: 14,
  },
}
