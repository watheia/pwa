import { createMuiTheme, responsiveFontSizes, Theme } from "@material-ui/core"
import { light, dark } from "./palette"
import { typography } from "./typography"

declare module "@material-ui/core/styles/createMuiTheme" {
  interface Theme {
    layout: {
      contentWidth: number | string
    }
  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    layout?: {
      contentWidth: number | string
    }
  }
}

import * as createPalette from "@material-ui/core/styles/createPalette"
declare module "@material-ui/core/styles/createPalette" {
  interface TypeBackground {
    paper: string
    default: string
    level2: string
    level1: string
    footer: string
  }

  interface PaletteOptions {
    cardShadow?: string
    alternate: {
      main: string
      dark: string
    }
  }

  interface Palette {
    cardShadow?: string
    alternate: {
      main: string
      dark: string
    }
  }
}

export type { Theme, ThemeOptions } from "@material-ui/core/styles/createMuiTheme"
export type {
  TypeBackground,
  PaletteOptions,
  Palette,
} from "@material-ui/core/styles/createPalette"

export const createTheme = (mode: "light" | "dark"): Theme =>
  responsiveFontSizes(
    createMuiTheme({
      palette: mode === "light" ? light : dark,
      layout: {
        contentWidth: 1236,
      },
      typography,
      zIndex: {
        appBar: 1200,
        drawer: 1100,
      },
    })
  )

export default createTheme
