import React, { useContext } from "react"
import { ThemeProvider } from "./theme-provider"
import { ThemeContext } from "./theme-context"

export function MockComponent() {
  const theme = useContext(ThemeContext)

  return (
    <div style={{ color: theme.mode === "dark" ? "#000000" : "#cccccc" }}>
      this should be {theme.mode}
    </div>
  )
}

export const DarkTheme = () => {
  return (
    <ThemeProvider mode="dark">
      <MockComponent />
    </ThemeProvider>
  )
}

export const LightTheme = () => {
  return (
    <ThemeProvider mode="light">
      <MockComponent />
    </ThemeProvider>
  )
}
