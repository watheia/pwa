import React from "react"

const EMBED_LINK = "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-icons.css"

export type IconFontProps = {
  /** query id for cache busting (copy this from Icomoon's _production_ link) */
  query?: string
} & React.LinkHTMLAttributes<HTMLLinkElement>

/**
 * Embeds the official icon font of [Watheia](https://watheia.io).
 * Place at the root element, and use [bit-icon](https://bit.dev/watheia/base-ui/theme/fonts/icon-font) to place icons anywhere in the page.
 * @name IconFont
 * @example
 * <html>
 * 	<head>
 * 		<IconFont query='aqq93z' />
 * 	<head>
 * </html>
 */
export function IconFont(props: IconFontProps) {
  const { query = "dqaogs" } = props
  return (
    <link
      data-bit-id="watheia.base-ui/theme/fonts/icon-font"
      rel="stylesheet"
      href={`${EMBED_LINK}?${query}`}
    />
  )
}
