import React from "react"
import { Anchor } from "./anchor"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { H1 } from "@watheia/base-ui.mdx.ui.heading"

export const AnchorComponentWithHeading = () => {
  return (
    <ThemeProvider>
      <div style={{ display: "inline-flex", alignItems: "center" }}>
        <H1 style={{ marginRight: "10px" }}>example</H1>
        <Anchor href="example" />
      </div>
    </ThemeProvider>
  )
}

export const SimpleAnchor = () => {
  return (
    <ThemeProvider>
      <Anchor href="example" />
    </ThemeProvider>
  )
}

SimpleAnchor.canvas = {
  width: 50,
}
