import React from "react"
import { LinkedHeading } from "./linked-heading"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const Large = () => {
  return (
    <ThemeProvider>
      <LinkedHeading size="xl" link="link">
        large
      </LinkedHeading>
    </ThemeProvider>
  )
}
export const Default = () => {
  return (
    <ThemeProvider>
      <LinkedHeading link="link">default</LinkedHeading>
    </ThemeProvider>
  )
}
export const Small = () => {
  return (
    <ThemeProvider>
      <LinkedHeading size="xs" link="link">
        small
      </LinkedHeading>
    </ThemeProvider>
  )
}
