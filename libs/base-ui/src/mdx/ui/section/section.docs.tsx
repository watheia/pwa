import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Section } from "./section"

export const labels = ["react", "ui-component", "container"]

export const examples = [
  {
    scope: {
      Section,
      ThemeProvider,
    },
    title: "Using the Section component.",
    code: `
<ThemeProvider>
    <Section className="custom-section">
        This is a section.
    </Section>
</ThemeProvider>
`,
  },
]
