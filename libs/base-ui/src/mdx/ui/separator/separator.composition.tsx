import React from "react"
import { Separator } from "./separator"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

// tODO - make text stay single line
export const SeapartorExample = () => {
  return (
    <ThemeProvider>
      <div>
        <Separator style={{ width: "100%" }} />
      </div>
    </ThemeProvider>
  )
}
