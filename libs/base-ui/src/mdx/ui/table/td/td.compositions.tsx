import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Td } from "./td"

export const TdExample = () => {
  return (
    <ThemeProvider>
      <Td>Td text</Td>
    </ThemeProvider>
  )
}
