import { useState } from "react"
import { CopiedMessage } from "@watheia/base-ui.mdx.ui.copied-message"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Icon } from "@watheia/base-ui.atoms.icon"

export const labels = ["react", "ui-component"]

export const abstract = "Indicates when a text has been copied."

export const examples = [
  {
    scope: {
      CopiedMessage,
      Icon,
      ThemeProvider,
      useState,
    },
    title: "Using the Copied Message component",
    description: "Use it to indicate when a text has been copied.",
    code: `
    () => {
        const [isCopied, setIsCopied] = useState(false);

        const handleClick = () => {
          setIsCopied(true);
          setTimeout(() => setIsCopied(false), 2000);
        };
        return (
          <ThemeProvider>
            <div style={{position: 'relative', width: 205}}>
                <p style={{display: 'inline'}}>Click on the icon to copy!</p>
                <CopiedMessage show={isCopied} style={{top: 0}} />
                <Icon
                    of="copy-cmp"
                    onClick={handleClick}
                    style ={{paddingLeft: "5px"}}
                    />
            </div>
          </ThemeProvider>
        );
      };
`,
  },
]

// export const HiddenCopiedMessageExample = () => {
//     const [isCopied, setIsCopied] = useState(false);

//     const handleClick = () => {
//       setIsCopied(true);
//       setTimeout(() => setIsCopied(false), 2000);
//     };
//     return (
//       <ThemeProvider>
//         <div>
//             <p>Click on the icon to copy!</p>
//             <Icon onClick={handleClick} of="copy-cmp" />
//             <CopiedMessage show={isCopied} />
//         </div>
//       </ThemeProvider>
//     );
//   };
