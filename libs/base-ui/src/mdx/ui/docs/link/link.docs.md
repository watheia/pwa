---
labels: ["react", "typescript", "ui", "link"]
description: "Links text to an external page."
---

import { Link } from './link';

Example:

```js live
<Link href="https://watheia.io">watheia.io</Link>
```
