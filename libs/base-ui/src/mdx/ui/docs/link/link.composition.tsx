import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Link } from "./link"

export const LinkExample = () => (
  <ThemeProvider>
    <Link data-testid="test-link" href="https://watheia.io">
      watheia.io
    </Link>
  </ThemeProvider>
)
