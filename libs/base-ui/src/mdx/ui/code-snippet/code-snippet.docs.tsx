import { CodeSnippet } from "@watheia/base-ui.mdx.ui.code-snippet"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const labels = ["react", "ui-component", "code snippet"]
export const abstract = "A syntax highlighter for React snippets."

export const examples = [
  {
    scope: {
      CodeSnippet,
      ThemeProvider,
    },
    title: "Using the Code Snippet",
    description: `The Code Snippet uses the react-syntax-highlighter library. Use it to add snippets of code to your documentation.`,
    code: `
<ThemeProvider>
    <div
    style={{
        display: 'inline-flex',
        alignItems: 'center',
        maxWidth: '600px',
    }}
    >
    <CodeSnippet>
        function HelloWorld () {}
    </CodeSnippet>
    </div>
</ThemeProvider>
`,
  },
]
