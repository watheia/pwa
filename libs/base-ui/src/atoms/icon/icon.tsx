import React from "react"

import { BaseIcon, BaseIconProps } from "@watheia/base-ui.atoms.base-icon"

const iconSetPrefix = "bitcon"

export type IconProps = BaseIconProps

/**
 * Placeholder for an icon, from Watheia's icon font.
 *
 * @name Icon
 * @example
 * // Embed icon at the document root:
 * <IconFont query="sdfj4k2d"/>
 *
 * // Then use icon anywhere in the document:
 * <Icon of="spinner"/>
 */
export function Icon({ className, of: iconName, ...rest }: IconProps) {
  return (
    <BaseIcon
      of={`${iconSetPrefix}-${iconName}`}
      className={className}
      data-bit-id="watheia.base-ui/atoms/icon"
      {...rest}
    />
  )
}
