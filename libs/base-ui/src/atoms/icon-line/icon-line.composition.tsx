import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { IconLine } from "./icon-line"

export const IconLineExample = () => (
  <ThemeProvider>
    <IconLine
      icons={[
        "logo-react",
        "logo-vue",
        "logo-angular",
        "logo-web-components",
        "logo-ts",
        "logo-js",
      ]}
      data-testid="test-iconline"
    />
  </ThemeProvider>
)

IconLineExample.canvas = {
  height: 90,
}
