import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { XButton } from "./x-button"

export const CloseButton = () => (
  <ThemeProvider>
    <XButton onClick={() => alert("on click")} data-testid="test-x" />
  </ThemeProvider>
)

CloseButton.canvas = {
  height: 90,
}
