import { Box } from "@material-ui/core"
import * as React from "react"
import { Copyright } from "."

export const CopyrightExample = (): JSX.Element => {
  return (
    <Box
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      padding={2}
      border="1px solid #ccc"
      borderRadius="4px"
      minWidth="md"
    >
      <Copyright url="https://watheia.io" label="Watheia Labs, LLC" />
    </Box>
  )
}
