import * as React from "react"
import { render } from "@testing-library/react"
import { DarkModeToggler } from "./DarkModeToggler"

describe("ui.atoms.DarkModeToggler", () => {
  it("should be rendered correctly", () => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const { asFragment } = render(<DarkModeToggler onClick={() => {}} />)
    expect(asFragment()).toMatchSnapshot()
  })
})
