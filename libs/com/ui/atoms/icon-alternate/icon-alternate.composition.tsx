import React from "react"
import { Box, colors } from "@material-ui/core"
import IconAlternate from "."

export function Example() {
  return (
    <Box
      marginBottom={2}
      padding={2}
      border="1px solid #ccc"
      borderRadius="4px"
      overflow="auto"
    >
      <Box display="flex" justifyContent="space-around" alignItems="center" minWidth="600px">
        <IconAlternate
          shape="circle"
          size="small"
          fontIconClass="far fa-address-book"
          color={colors.purple}
        />
        <IconAlternate fontIconClass="fas fa-users" color={colors.pink} />
        <IconAlternate fontIconClass="fas fa-quote-right" size="medium" color={colors.blue} />
        <IconAlternate fontIconClass="fas fa-phone-alt" size="large" color={colors.green} />
      </Box>
    </Box>
  )
}
