import React from "react"
import { Box, colors } from "@material-ui/core"
import Icon from "."

export const IconExample = (): JSX.Element => {
  return (
    <Box
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      padding={2}
      border="1px solid #ccc"
      borderRadius="4px"
    >
      <Icon fontIconClass="fas fa-users" fontIconColor={colors.pink[50]} />
      <Icon
        fontIconClass="far fa-address-book"
        size="small"
        fontIconColor={colors.purple[500]}
      />
      <Icon fontIconClass="fab fa-angellist" size="medium" fontIconColor={colors.blue[500]} />
      <Icon fontIconClass="fas fa-phone-alt" size="large" fontIconColor={colors.green[500]} />
    </Box>
  )
}
