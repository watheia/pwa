import React from "react"
import { Box, colors } from "@material-ui/core"
import IconText from "."

export function Example() {
  return (
    <Box
      marginBottom={2}
      padding={2}
      border="1px solid #ccc"
      borderRadius="4px"
      overflow="auto"
    >
      <Box display="flex" justifyContent="space-between" alignItems="center" minWidth="650px">
        <IconText
          fontIconClass="fas fa-users"
          color={colors.pink[50]}
          title="Users Icon Text"
        />
        <IconText
          fontIconClass="far fa-address-book"
          color={colors.purple[500]}
          title="Book Icon Text"
        />
        <IconText
          fontIconClass="fab fa-angellist"
          color={colors.blue[500]}
          title="Cool Icon Text"
        />
        <IconText
          fontIconClass="fas fa-phone-alt"
          color={colors.green[500]}
          title="Phone Icon Text"
        />
      </Box>
    </Box>
  )
}
