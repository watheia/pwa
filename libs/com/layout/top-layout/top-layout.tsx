import React, { HTMLAttributes } from "react"
import { Helmet } from "react-helmet"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export type LayoutProps = {
  /**
   * The title set in head and seo meta data
   */
  title?: string

  /**
   * The description to use seo meta data
   */
  description?: string

  /**
   * An optional canonical url for the page
   */
  canonical: string

  /**
   * An optional locale to use for the page
   */
  locale?: string

  /** handles 'see demo' call to action */
  onDemoMode?: () => unknown | Promise<unknown>

  /**
   * The url of an  optional image to use for link previews
   */
  imageUrl?: string
} & HTMLAttributes<HTMLDivElement>

export const displayName = "TopLayout"

export const defaultPalette = "light"

/**
 * Component to display the background hero
 *
 * @param {Object} props
 */
export const TopLayout: React.FC<LayoutProps> = ({
  children,
  title = "Micro Frontends by Watheia Labs",
  description = "Watheia Labs, LLC. is a modern engineerign and digital design agency located in Southeast Washington State",
  canonical,
  locale = "en_US",
  imageUrl = "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-card-bgxxhdpi.png",
  ...props
}): JSX.Element => {
  return (
    <div {...props}>
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="https://gitlab.com/watheia/pwa/-/raw/main/assets/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="https://gitlab.com/watheia/pwa/-/raw/main/assets/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="https://gitlab.com/watheia/pwa/-/raw/main/assets/favicon-16x16.png"
        />
        <link
          rel="manifest"
          href="https://gitlab.com/watheia/pwa/-/raw/main/assets/site.webmanifest"
        />
        <link
          rel="shortcut icon"
          href="https://gitlab.com/watheia/pwa/-/raw/main/assets/icon.png"
        />
        <meta name="theme-color" content="#092947" />
        <meta name="description" content={description} />
        <meta
          name="robots"
          content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"
        />
        <meta property="og:locale" content={locale} />
        <meta property="og:type" content="website" />
        <meta property="og:image" content={imageUrl} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="og:url" content={canonical} />
        <script
          src="https://kit.fontawesome.com/4c273e6d43.js"
          crossOrigin="anonymous"
        ></script>
        <title>{title}</title>
      </Helmet>
      <ThemeProvider>{children}</ThemeProvider>
    </div>
  )
}

export default TopLayout
