import { displayName, TopLayout, LayoutProps } from "./top-layout"

export { displayName, TopLayout }

export type { LayoutProps }
export default TopLayout
