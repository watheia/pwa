import React from "react"
// import NxtLink from "next/link"
import { Link as MuiLink, LinkProps } from "@material-ui/core"

// TODO use forward ref in nextjs link
export const Link = ({ children, ...rest }: LinkProps) => (
  <MuiLink {...rest}>{children}</MuiLink>
)

export default Link
