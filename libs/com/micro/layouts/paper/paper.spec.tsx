import React from "react"
import { render } from "@testing-library/react"
import { PaperLayoutExample } from "./paper.composition"

describe("paper", () => {
  it("should render with the correct text", () => {
    const { getByText } = render(<PaperLayoutExample />)
    const rendered = getByText("Sign in")
    expect(rendered).toBeTruthy()
  })
})
