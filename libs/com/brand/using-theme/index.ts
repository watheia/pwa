export type { Theme, ThemeOptions } from "./createMuiTheme"
export type { TypeBackground, PaletteOptions, Palette } from "./createPalette"
import usingTheme from "./using-theme"
import { light, dark } from "./palette"

const palette = { light, dark }

export { usingTheme, palette }
export default usingTheme
