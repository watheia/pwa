import { createMuiTheme, responsiveFontSizes, Theme } from "@material-ui/core"
import { light, dark } from "./palette"

const usingTheme = (mode: "light" | "dark"): Theme =>
  responsiveFontSizes(
    createMuiTheme({
      palette: mode === "light" ? light : dark,
      layout: {
        contentWidth: 1236,
      },
      zIndex: {
        appBar: 1200,
        drawer: 1100,
      },
      typography: {
        fontFamily: [
          "-apple-system",
          "BlinkMacSystemFont",
          '"Segoe UI"',
          "Roboto Slab",
          '"Helvetica Neue"',
          "Arial",
          "sans-serif",
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
        ].join(","),
      },
    })
  )

export default usingTheme
