/**
 * Copyright 2021 Watheia Labs LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const SITE_URL = "https://watheia.io"

export const SITE_ORIGIN = process.env.NEXT_PUBLIC_SITE_ORIGIN || new URL(SITE_URL).origin

export const TWITTER_USER_NAME = "watheia"

export const BRAND_NAME = "Watheia"

export const SITE_NAME_MULTILINE = ["Watheia", "Labs"]

export const SITE_NAME = "Micro Frontends by Watheia Labs"

export const META_DESCRIPTION =
  "Watheia Labs, LLC. is a modern engineerign and digital design agency located in Southeast Washington State"

export const SITE_DESCRIPTION = "Micro Frontends by Watheia Labs"

export const TWEET_TEXT = META_DESCRIPTION

export const COOKIE = "user-id"

export const NEXT_PUBLIC_DATOCMS_API_TOKEN = "9049ce0b858dbf7a01daa51b8eb3d1"

// Remove process.env.NEXT_PUBLIC_... below and replace them with
// strings containing your own privacy policy URL and copyright holder name
export const LEGAL_URL =
  process.env.NEXT_PUBLIC_PRIVACY_POLICY_URL ||
  "https://gitlab.com/watheia/pwa/-/raw/main/assets/terms-and-conditions.txt"
export const COPYRIGHT_HOLDER = "Watheia Labs, LLC"

export const REPO = "https://gitlab.com/watheia/pwa"

export const ROUTES = [
  {
    name: "Home",
    route: "/home",
  },
  {
    name: "Micro",
    route: "/micro",
  },
  {
    name: "Support",
    route: "/support",
  },
]

export type PrincipalState = "guest" | "member"
