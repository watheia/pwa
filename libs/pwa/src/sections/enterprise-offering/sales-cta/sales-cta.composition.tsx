import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { SalesCta } from "./sales-cta"

export const SalesCtaExample = () => (
  <ThemeProvider>
    <SalesCta data-testid="test-sales-cta" />
  </ThemeProvider>
)

SalesCtaExample.canvas = {
  height: 650,
}
