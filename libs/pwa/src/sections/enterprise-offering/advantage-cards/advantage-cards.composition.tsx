import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { AdvantageCards } from "./advantage-cards"

export const AdvantageCardsExample = () => (
  <ThemeProvider>
    <AdvantageCards data-testid="test-cards" />
  </ThemeProvider>
)

AdvantageCardsExample.canvas = {
  width: 1200,
  height: 400,
  overflow: "scroll",
}
