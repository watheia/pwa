import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { EcoSystem } from "./eco-system"

export const EcoSystemExample = () => (
  <ThemeProvider>
    <EcoSystem data-testid="test-ecosystem" />
  </ThemeProvider>
)

EcoSystemExample.canvas = {
  height: 120,
}
