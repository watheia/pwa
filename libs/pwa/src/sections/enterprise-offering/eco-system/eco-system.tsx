import React, { HTMLAttributes } from "react"
import classNames from "classnames"

import { Paragraph } from "@watheia/base-ui.text.paragraph"
import { PossibleSizes } from "@watheia/base-ui.theme.sizes"
import { mutedText } from "@watheia/base-ui.text.muted-text"

import { H2 } from "@watheia/base-ui.atoms.heading"
import { textColumn } from "@watheia/base-ui.layout.page-frame"
import { marginCenter } from "@watheia/base-ui.layout.align"

/**
 * Title and description to the Bit integrations ecosystem.
 * @name EcoSystem
 */
export const EcoSystem = (props: HTMLAttributes<HTMLDivElement>) => {
  return (
    <div
      data-testid="ecosystem"
      {...props}
      data-bit-id="watheia.pwa/sections/enterprise-offering/eco-system"
    >
      <H2 data-testid="ecosystem_title" size={PossibleSizes.sm}>
        Disrupt the web monolith!
      </H2>
      <Paragraph
        data-testid="ecosystem_content"
        className={classNames(mutedText, textColumn, marginCenter)}
        size={PossibleSizes.lg}
      >
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed debitis, animi modi
        adipisci odit voluptas molestias ipsam aperiam quo dicta. Eius reprehenderit expedita
        aut incidunt est velit harum itaque unde..
      </Paragraph>
    </div>
  )
}
