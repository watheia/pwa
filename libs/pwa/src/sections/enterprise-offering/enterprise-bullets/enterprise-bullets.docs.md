---
labels: ["react", "typescript", "section", "bullets"]
description: "microfrontend bullets section"
---

A section showing few advantages of microfrontend solutions.
