import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Hero } from "./hero"

export const HeroExample = () => (
  <ThemeProvider>
    <Hero />
  </ThemeProvider>
)

HeroExample.canvas = {
  height: 520,
}
