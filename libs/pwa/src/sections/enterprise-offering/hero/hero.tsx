import React, { HTMLAttributes } from "react"
import classNames from "classnames"

import { Paragraph } from "@watheia/base-ui.text.paragraph"
import { themedText } from "@watheia/base-ui.text.themed-text"
import { PossibleSizes } from "@watheia/base-ui.theme.sizes"
import { text, marginCenter } from "@watheia/base-ui.layout.align"
import { Grid } from "@watheia/base-ui.layout.grid-component"

import { Button } from "@watheia/base-ui.atoms.button"
import { H1 } from "@watheia/base-ui.atoms.heading"
import { Image } from "@watheia/base-ui.atoms.image"

import styles from "./hero.module.scss"

type HeroProps = {
  /** handles 'book meeting' call to action */
  onDemoMode?: () => void | Promise<void>
} & HTMLAttributes<HTMLDivElement>

/**
 * Opening section for the Enterprise offering page.
 * @name EcoSystem
 */
export function Hero(props: HeroProps) {
  const { onDemoMode, ...rest } = props

  return (
    <Grid
      colL={2}
      {...rest}
      data-testid="hero"
      data-bit-id="watheia.pwa/sections/enterprise-offering/hero"
      className={classNames(props.className, text.center, text.l.left, styles.mainGrid)}
    >
      <div className={classNames(styles.content)}>
        <H1 data-testid="hero_title" size={PossibleSizes.sm} className={themedText}>
          We build micro frontends!
        </H1>
        <Paragraph
          data-testid="hero_content"
          size={PossibleSizes.lg}
          className={styles.paragraph}
        >
          Let Watheia Labs drive web application delivery at global scale. Enjoy world-class
          performance, security and support every step of the way.
        </Paragraph>

        <div data-testid="hero-actions" className={styles.buttons}>
          <a href="/contact">
            <Button data-testid="hero-actions_contact" importance="cta">
              Talk to Us
            </Button>
          </a>
          <div className={styles.spacer} />
          <Button data-testid="hero-actions_demo" onClick={props.onDemoMode}>
            Try Our Demo
          </Button>
        </div>
      </div>
      <Image
        src="/micro/hero-enterprise.svg"
        alt="compartmentalized corporate ui"
        className={classNames(marginCenter, styles.image)}
      />
    </Grid>
  )
}
