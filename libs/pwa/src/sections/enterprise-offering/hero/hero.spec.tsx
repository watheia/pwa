import React from "react"
import { render } from "@testing-library/react"
import { expect } from "chai"

import { HeroExample } from "./hero.composition"

it("should render correctly", () => {
  const { getByTestId } = render(<HeroExample />)
  const rendered = getByTestId("hero")

  expect(rendered).to.be.ok
})
it("should render h2 element", () => {
  const { getByTestId } = render(<HeroExample />)
  const rendered = getByTestId("hero_title")

  expect(rendered.tagName).to.be.equal("H1")
})
it("should render p element", () => {
  const { getByTestId } = render(<HeroExample />)
  const rendered = getByTestId("hero_content")

  expect(rendered.tagName).to.be.equal("P")
})
