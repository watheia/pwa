import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ComponentAnalytics } from "./component-analytics"

export const ComponentAnalyticsExample = () => (
  <ThemeProvider>
    <ComponentAnalytics data-testid="test-analytics" />
  </ThemeProvider>
)

ComponentAnalyticsExample.canvas = {
  height: 350,
}
