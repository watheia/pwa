import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Integrations } from "./integrations"

export const IntegrationsExample = () => (
  <ThemeProvider>
    <Integrations data-testid="test-integrations" />
  </ThemeProvider>
)

IntegrationsExample.canvas = {
  height: 180,
}
