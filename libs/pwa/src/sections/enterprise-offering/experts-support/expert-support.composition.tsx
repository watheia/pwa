import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ExpertSupport } from "./expert-support"

export const ExpertSupportExample = () => (
  <ThemeProvider>
    <ExpertSupport data-testid="test-expert-support" />
  </ThemeProvider>
)

ExpertSupportExample.canvas = {
  height: 650,
}
