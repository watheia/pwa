---
labels: ["react", "typescript", "enterprise"]
description: "Bit summary solution"
---

### Overview

A section showing a summary of our microfrontend solution.  
[Link to page](https://bit.dev/enterprise).
