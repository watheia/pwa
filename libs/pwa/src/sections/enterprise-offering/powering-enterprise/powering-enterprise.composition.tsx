import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { PoweringEnterprise } from "./powering-enterprise"

export const PoweringEnterpriseExample = () => (
  <ThemeProvider>
    <PoweringEnterprise data-testid="test-powering" />
  </ThemeProvider>
)

PoweringEnterpriseExample.canvas = {
  height: 350,
}
