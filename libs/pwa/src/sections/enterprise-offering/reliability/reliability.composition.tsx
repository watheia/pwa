import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Reliability } from "./reliability"

export const ReliabilityExample = () => (
  <ThemeProvider>
    <Reliability data-testid="test-reliability" />
  </ThemeProvider>
)

ReliabilityExample.canvas = {
  height: 650,
}
