import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ReuseComponents } from "./reuse-components"

export const ReuseComponentsExample = () => (
  <ThemeProvider>
    <ReuseComponents data-testid="test-reuse" />
  </ThemeProvider>
)

ReuseComponentsExample.canvas = {
  width: 1400,
  height: 600,
  overflow: "scroll",
}
