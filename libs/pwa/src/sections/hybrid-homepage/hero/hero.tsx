import React, { HTMLAttributes, ReactNode } from "react"
import classNames from "classnames"
import Typewriter from "typewriter-effect"

import { PossibleSizes } from "@watheia/base-ui.theme.sizes"
import { text, marginCenter } from "@watheia/base-ui.layout.align"
import { Paragraph } from "@watheia/base-ui.text.paragraph"
import { Grid } from "@watheia/base-ui.layout.grid-component"
import { textColumn } from "@watheia/base-ui.layout.page-frame"

import { H1 } from "@watheia/base-ui.atoms.heading"
import { IconLine } from "@watheia/base-ui.atoms.icon-line"
import { Link } from "@watheia/base-ui.atoms.link"
import { Image } from "@watheia/base-ui.atoms.image"
import { Button } from "@watheia/base-ui.atoms.button"
import styles from "./hero.module.scss"

const iconsArray = [
  "logo-react",
  "logo-vue",
  "logo-angular",
  "logo-web-components",
  "logo-ts",
  "logo-js",
]

const typeWriterArray = [
  "micro components",
  "micro services",
  "micro frontends",
  "micro everything!",
]

type ReuseLandingProps = {
  /**
   * Placeholder for content, at the bottom of this section
   */
  mainCta?: ReactNode
} & HTMLAttributes<HTMLDivElement>

/**
 * @name Hero
 * @description
 * The Main and first section the user sees as he enters the page.
 * Assumes a dark purplish background behind it.
 */
export function Hero(props: ReuseLandingProps) {
  return (
    <div data-testid="hero" data-bit-id="watheia.pwa/sections/hero" {...props}>
      <Grid colL={2} className={classNames(styles.mainGrid, text.center, text.l.left)}>
        <div>
          <div>
            <H1 data-testid="hero_title" size={PossibleSizes.sm} className={styles.headline}>
              We build
              <br />
              <Typewriter
                onInit={() => {
                  return void 0
                }}
                options={{
                  strings: typeWriterArray,
                  autoStart: true,
                  loop: true,
                  delay: 50,
                  deleteSpeed: 50,
                }}
              />
            </H1>
            <Paragraph
              data-testid="hero_content"
              size={PossibleSizes.lg}
              className={classNames(styles.paragraph, textColumn, marginCenter)}
            >
              Watheia Labs LLC is a modern engineering and digital design agency offering
              consulting services in Southeast Washington State.
            </Paragraph>
          </div>

          <Paragraph
            data-testid="hero_actions"
            size={PossibleSizes.sm}
            element="div"
            className={classNames(styles.buttons)}
          >
            <Link href="https://watheia.io/support">
              <Button elevation="medium">Free Consultation</Button>
            </Link>
            <div className={styles.spacer} />
            <Link href="https://gitlab.com/watheia/pwa">
              <Button importance="cta" elevation="medium">
                Reference Project
              </Button>
            </Link>
          </Paragraph>
          <IconLine icons={iconsArray} className={styles.icons} />
        </div>

        <Image
          src="/home/process-2.svg"
          className={marginCenter}
          alt="illustration"
          fullWidth
        />
      </Grid>
    </div>
  )
}
