import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { DiscoverComponents } from "./discover-components"

export const DiscoverComponentsExample = () => (
  <ThemeProvider>
    <DiscoverComponents data-testid="test-discover" />
  </ThemeProvider>
)

DiscoverComponentsExample.canvas = {
  width: 1400,
  height: 400,
  overflow: "scroll",
}
