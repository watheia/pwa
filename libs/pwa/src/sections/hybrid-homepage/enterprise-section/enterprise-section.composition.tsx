import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { EnterpriseSection } from "./enterprise-section"

export const EnterpriseSectionExample = () => (
  <ThemeProvider>
    <EnterpriseSection data-testid="test-enterprise" />
  </ThemeProvider>
)

EnterpriseSectionExample.canvas = {
  width: 1400,
  height: 400,
  overflow: "scroll",
}
