import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { CommunitySection } from "./community-section"

export const CommunitySectionExample = () => (
  <ThemeProvider>
    <CommunitySection githubStars={12600} data-testid="community-test" />
  </ThemeProvider>
)

CommunitySectionExample.canvas = {
  height: 430,
}
