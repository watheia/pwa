import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { BuildTogether } from "./build-together"

export const BuildTogetherExample = () => (
  <ThemeProvider>
    <BuildTogether data-testid="test-build" />
  </ThemeProvider>
)

BuildTogetherExample.canvas = {
  width: 1400,
  height: 400,
  overflow: "scroll",
}
