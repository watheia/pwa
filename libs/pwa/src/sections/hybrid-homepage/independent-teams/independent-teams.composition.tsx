import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { IndependentTeams } from "./independent-teams"

export const IndependentTeamsExample = () => (
  <ThemeProvider>
    <IndependentTeams data-testid="test-teams" />
  </ThemeProvider>
)

IndependentTeamsExample.canvas = {
  width: 1400,
  height: 600,
  overflow: "scroll",
}
