import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { TalkToExperts } from "./talk-to-experts"

export const TalkToExpertsExample = () => (
  <ThemeProvider>
    <TalkToExperts data-testid="test-talk" />
  </ThemeProvider>
)

TalkToExpertsExample.canvas = {
  width: 1400,
  height: 300,
  overflow: "scroll",
}
