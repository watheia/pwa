import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { GlobalEnterprise } from "./global-enterprise"

export const GlobalEnterpriseExample = () => (
  <ThemeProvider>
    <GlobalEnterprise data-testid="test-global" />
  </ThemeProvider>
)

GlobalEnterpriseExample.canvas = {
  width: 1400,
  height: 700,
  overflow: "scroll",
}
