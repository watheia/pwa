import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { SupportChannels } from "./support-channels"

export const SupportChannelsExample = () => (
  <ThemeProvider>
    <SupportChannels data-testid="test-support" />
  </ThemeProvider>
)

SupportChannelsExample.canvas = {
  width: 1400,
  height: 550,
  overflow: "scroll",
}
