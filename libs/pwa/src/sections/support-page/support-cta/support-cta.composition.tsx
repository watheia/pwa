import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { SupportCta } from "./support-cta"

export const SupportCtaExample = () => (
  <ThemeProvider>
    <SupportCta data-testid="test-support" />
  </ThemeProvider>
)
