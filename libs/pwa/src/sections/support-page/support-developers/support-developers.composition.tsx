import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { SupportDevelopers } from "./support-developers"

export const SupportDevelopersExample = () => (
  <ThemeProvider>
    <SupportDevelopers data-testid="test-support" />
  </ThemeProvider>
)
