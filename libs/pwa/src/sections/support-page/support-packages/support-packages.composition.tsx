import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { SupportPackages } from "./support-packages"

export const SupportPackagesExample = () => (
  <ThemeProvider>
    <SupportPackages data-testid="test-support">
      support comparison table should be passed
    </SupportPackages>
  </ThemeProvider>
)
