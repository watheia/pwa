import React from "react"
import { TopLayout } from "@watheia/com.layout.top-layout"
import { EnterpriseOffering } from "./enterprise-offering"

export const EnterpriseOfferingPage = () => (
  <TopLayout canonical="https://watheia.io/micro">
    <EnterpriseOffering data-testid="test-page" />
  </TopLayout>
)

EnterpriseOfferingPage.canvas = {
  width: 1200,
  height: 400,
  overflow: "scroll",
}
