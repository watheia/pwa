import React, { HTMLAttributes } from "react"
import classNames from "classnames"

import { centerColumn, textColumn } from "@watheia/base-ui.layout.page-frame"
import { marginCenter, text } from "@watheia/base-ui.layout.align"
import { backgrounds } from "@watheia/base-ui.surfaces.background"

import { Hero } from "@watheia/pwa.sections.enterprise-offering.hero"
import { PoweringEnterprise } from "@watheia/pwa.sections.enterprise-offering.powering-enterprise"
import { AdvantageCards } from "@watheia/pwa.sections.enterprise-offering.advantage-cards"
import { Reliability } from "@watheia/pwa.sections.enterprise-offering.reliability"
import { EcoSystem } from "@watheia/pwa.sections.enterprise-offering.eco-system"
import { Integrations } from "@watheia/pwa.sections.enterprise-offering.integrations"
import { ComponentAnalytics } from "@watheia/pwa.sections.enterprise-offering.component-analytics"
import { ExpertSupport } from "@watheia/pwa.sections.enterprise-offering.experts-support"
import { SalesCta, ContactValues } from "@watheia/pwa.sections.enterprise-offering.sales-cta"
import { EnterpriseBullets } from "@watheia/pwa.sections.enterprise-offering.enterprise-bullets"
import { margin } from "@watheia/base-ui.theme.spacing"
import { EnterpriseLogos } from "@watheia/pwa.marketing.enterprise-logos"

import styles from "./enterprise-offering.module.scss"

export type EnterpriseOfferingProps = {
  /** handles "contact us" form submission. Return a promise to show loader */
  onSubmitCta?: (values: ContactValues) => void | Promise<void>
  /** handles demo call to action. Return a promise to show loader */
  onDemoMode?: () => void | Promise<void>
} & HTMLAttributes<HTMLDivElement>

/**
 * A full, responsive page, detailing Watheia's offering for enterprises.
 * @name EnterpriseOffering
 */
export function EnterpriseOffering(props: EnterpriseOfferingProps) {
  const { onDemoMode, onSubmitCta, className, ...rest } = props

  return (
    <div
      {...rest}
      className={classNames(styles.enterpriseOffering, backgrounds.bedrock, className)}
    >
      <div className={classNames(styles.gradient00, styles.paddingTop130)}>
        <Hero onDemoMode={onDemoMode} className={classNames(centerColumn, styles.foldMargin)} />

        <div className={classNames(styles.curveWhite, styles.curveMargin)} />
      </div>

      <PoweringEnterprise className={classNames(centerColumn, margin[180])} />

      <EnterpriseLogos className={styles.margin160} />

      <EnterpriseBullets className={classNames(centerColumn, margin[180])} />

      <div className={styles.gradient01}>
        <AdvantageCards className={classNames(centerColumn, styles.foldMarginPlus)} />

        <div className={classNames(styles.curveWhite, styles.curveMargin)} />
      </div>

      <Reliability
        className={classNames(centerColumn, styles.foldMargin, styles.paddingTop30)}
      />

      <div className={classNames(styles.curveCloud, styles.curveMargin)} />

      <div className={classNames(styles.gradient02, styles.paddingTop30)}>
        <EcoSystem className={classNames(centerColumn, text.center)} />
        <Integrations className={classNames(margin[80])} />
        <ComponentAnalytics className={centerColumn} />
      </div>
      <div className={classNames(styles.supportCirclesBg, styles.circlesMargin)}>
        <ExpertSupport className={classNames(textColumn, marginCenter, styles.expertSupport)} />
        <SalesCta
          onSubmitCta={onSubmitCta}
          onDemoMode={onDemoMode}
          className={classNames(marginCenter, centerColumn)}
        />
      </div>
    </div>
  )
}
