import React from "react"
import { TopLayout } from "@watheia/com.layout.top-layout"
import { HybridHomepage } from "./hybrid-homepage"

export const HybridHomePageComposition = () => (
  <TopLayout canonical="https://watheia.io/home">
    <HybridHomepage
      githubStars={12600}
      onDemoMode={() => alert("function to enable demo mode")}
    />
  </TopLayout>
)

HybridHomePageComposition.canvas = {
  width: 1200,
  height: 400,
  overflow: "scroll",
}
