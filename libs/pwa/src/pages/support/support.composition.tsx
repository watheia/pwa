import React from "react"
import { SupportPage } from "./support"
import { TopLayout } from "@watheia/com.layout.top-layout"

export const SupportPageComposition = () => (
  <TopLayout canonical="https://watheia.io/support">
    <SupportPage data-testid="test-page" />
  </TopLayout>
)

SupportPageComposition.canvas = {
  width: 1200,
  height: 400,
  overflow: "scroll",
}
