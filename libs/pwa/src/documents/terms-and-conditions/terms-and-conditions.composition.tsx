import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { MDXLayout } from "@watheia/base-ui.mdx.mdx-layout"
import { TermsAndConditions } from "./index"

export const TermsAndConditionsDocument = () => (
  <ThemeProvider>
    <MDXLayout>
      <TermsAndConditions />
    </MDXLayout>
  </ThemeProvider>
)
