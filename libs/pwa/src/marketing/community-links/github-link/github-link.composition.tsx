import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { GithubLink } from "./github-link"

export const GithubLinkExample = () => (
  <ThemeProvider>
    <GithubLink
      href="https://gitlab.com/watheia/pwa"
      starCount={500000}
      data-testid="test-link"
    />
  </ThemeProvider>
)

GithubLinkExample.canvas = {
  height: 90,
}
