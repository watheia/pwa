import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { CommunityLink } from "./community-link"

export const BaseCommunityLink = () => (
  <ThemeProvider>
    <CommunityLink href="https://bit.dev" data-testid="test-link" external>
      bit.dev
    </CommunityLink>
  </ThemeProvider>
)

BaseCommunityLink.canvas = {
  height: 90,
}
