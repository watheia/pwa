import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { TwitterLink } from "./twitter-link"

export const TwitterLinkExample = () => (
  <ThemeProvider>
    <TwitterLink href="https://twitter.com/bitdev_" data-testid="test-link" />
  </ThemeProvider>
)

TwitterLinkExample.canvas = {
  height: 90,
}
