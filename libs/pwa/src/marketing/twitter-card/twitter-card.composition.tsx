import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { TwitterCard } from "./twitter-card"

export const TwitterCardExample = () => (
  <ThemeProvider style={{ maxWidth: 250 }}>
    <TwitterCard
      title="@watheia_"
      image="https://gitlab.com/watheia/pwa/-/raw/main/assets/android-chrome-512x512.png"
      verified={true}
      data-testid="test-twitter-card"
    >
      This is my tweet
    </TwitterCard>
  </ThemeProvider>
)
