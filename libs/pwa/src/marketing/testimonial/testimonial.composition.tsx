import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Testimonial } from "./testimonial"

export const TestimonialExample = () => (
  <ThemeProvider>
    <Testimonial
      active
      data={{
        name: "Barbra",
        description: "head of barbering",
        content: "Great scissors!",
        avatar: "https://gitlab.com/watheia/pwa/-/raw/main/assets/mstile-150x150.png",
      }}
      data-testid="test-testimonial"
    />
  </ThemeProvider>
)
