import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { CommentCarousel } from "./comment-carousel"

export const CommentCarouselExample = () => {
  const bitTestimonials = [
    {
      name: "Barbra",
      description: "Quality assurance",
      content: "Better than 4/5 of the market",
      avatar: "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-logo.svg",
    },
    {
      name: "Roberto",
      description: "Upper middle management",
      content: "Easy to carry, nice for meetings",
      avatar: "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-logo.svg",
    },
    {
      name: "Barbra",
      description: "Quality assurance",
      content: "Better than 4/5 of the market",
      avatar: "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-logo.svg",
    },
    {
      name: "Roberto",
      description: "Upper middle management",
      content: "Easy to carry, nice for meetings",
      avatar: "https://gitlab.com/watheia/pwa/-/raw/main/assets/wa-logo.svg",
    },
  ]
  return (
    <ThemeProvider style={{ width: 800 }}>
      <CommentCarousel content={bitTestimonials} data-testid="test-carousel" />
    </ThemeProvider>
  )
}
