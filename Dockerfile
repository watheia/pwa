FROM node:lts-buster-slim

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

ENV PATH=$PATH:$HOME/bin
RUN echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc

# Upgrade base sys
RUN apt-get update && \
  apt-get upgrade -y && \
  apt-get install bash graphviz git git-lfs -y
# Install deps + add Chrome Stable + purge all the things

COPY scripts/install-chrome.sh /tmp/install-chrome.sh

RUN chmod +x /tmp/install-chrome.sh && \
  bash -C /tmp/install-chrome.sh && \
  rm /tmp/install-chrome.sh

# Add Chrome as a user
RUN groupadd -r wa && useradd -r -g wa -G audio,video wa \
  && mkdir -p /home/wa/app \
  && mkdir -p /home/wa && chown -R wa:wa /home/wa

RUN npm install --global @teambit/bvm

USER wa
WORKDIR /home/wa/app
COPY --chown=wa:wa . .

# start bit dev server
RUN export PATH=$PATH:$HOME/bin && \
  bvm install && \
  bit config set analytics_reporting false && \
  bit config set error_reporting false && \
  bit config set no_warnings true && \
  bit init --harmony && \
  bit import

CMD [ "bit", "--help" ]
